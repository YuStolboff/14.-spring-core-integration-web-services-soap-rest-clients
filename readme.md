#Practice task

1. Convert one of @repository class from JPA to JDBCTemplate
2. Add caching using Spring Cache Abstraction for this class
3. Create SOAP-service for one of domain objects, for example list of products.
    * Implement CRUD operations (Create product, Read product, Update product ((star)), Delete product)
    * Implement a search objects by attributes
    * The application must be thread-safe
4. Run app using Spring Boot.
5. Create a consumer for SOAP-service created in previous task, log the result of consuming
6. Create a consumer for REST-service created in Spring MVC module, log the result of consuming